import ply.yacc as yacc
from functools import reduce


from plog import *

        
####################################################################


# Get the token map from the lexer.
from pl_lexer import tokens

precedence = (
    # ('nonassoc', 'LT', 'LE', 'GT', 'GE', 'EQL', 'NEQL'),
    ('left', '+', '-'),
    ('left', '*', '/'),
)

def p_plsource(p):
    """
    plsource : empty
             | plinput
             | plsource plinput
    """
    if len(p) <= 2:
        p[0] = [p[1]] #...
    else:
        p[0] = p[1] + [p[2]]

def p_plinput(p):
    """
    plinput : rule
            | pred
            | query
    """
    p[0] = p[1]

def p_rule(p):
    """
    rule : term SEP nonemptyseq '.'
    """
    p[0] = Rule(p[1], p[3])

def p_pred(p):
    """
    pred : term '.'
    """
    p[0] = Fact(p[1])
    
def p_query(p):
    """
    query : term '?'
    """
    p[0] = Query(p[1])

def p_term(p):
    """
    term : atom
         | number
         | variable
         | complex
    """
    p[0] = p[1]

def p_complex(p):
    """
    complex : complexterm
            | list
            | complexarithexpr
            | boolexpr
            | isexpr
    """
    p[0] = p[1]
    
def p_complexterm(p):
    """
    complexterm : atom '(' seq ')'
    """
    p[0] = ComplexTerm(p[1], p[3])

def p_list(p):
    """
    list : '[' seq ']'
         | '[' nonemptyseq '|' list ']'
         | '[' nonemptyseq '|' variable ']'
         | '.' '(' term ',' term ')'
    """
    if len(p) <= 6:
        p[0] = reduce(lambda x, y: List(Atom('.'), [y, x]), reversed(p[2]), nil if len(p) <= 4 else p[4]) # bonne solution ?
    else:
        #print('len:', len(p))
        #print([p[i] for i in range(len(p))])
        p[0] = List([p[3], p[5]])
    

def p_seq(p):
    """
    seq : empty
        | nonemptyseq
    """
    p[0] = p[1] if p[1] else []

def p_nonemptyseq(p):
    """
    nonemptyseq : term
                | term ',' nonemptyseq
    """
    if len(p) <= 2:
        p[0] = [p[1]]
    else:
        p[0] = [p[1]] + p[3]

def p_empty(p):
    'empty :'
    pass
    
def p_boolexpr(p):
    """
    boolexpr : arithexpr boolop arithexpr
             | boolop '(' arithexpr ',' arithexpr ')'
    """
    if len(p) <= 4:
        p[0] = BoolExpr(Op(p[2]), [p[1], p[3]])
    else:
        p[0] = BoolExpr(Op(p[1]), [p[3], p[5]])

def p_boolop(p):
    """
    boolop : EQL
           | NEQL
           | LT
           | LE
           | GT
           | GE
    """
    p[0] = p[1]

def p_arithop(p):
    """
    arithop : '+'
            | '-'
            | '*'
            | '/'
    """
    p[0] = p[1]
    
def p_complexarithexpr(p):
    """
    complexarithexpr : arithexpr '+' arithexpr
                     | arithexpr '-' arithexpr
                     | arithexpr '*' arithexpr
                     | arithexpr '/' arithexpr
                     | arithop '(' arithexpr ',' arithexpr ')'
    """
    if len(p) <= 2:
        p[0] = p[1]
    elif len(p) <= 4:
        p[0] = ArithExpr(Op(p[2]), [p[1], p[3]])
    else:
        p[0] = ArithExpr(Op(p[1]), [p[3], p[5]])
    
def p_arithexpr(p):
    """
    arithexpr : number
              | variable
              | complexarithexpr
    """
    p[0] = p[1]
    
def p_expr(p):
    """
    expr : boolexpr
         | arithexpr
    """
    p[0] = p[1]
    
def p_isexpr(p):
    """
    isexpr : IS '(' variable ',' expr ')'
           | variable IS expr
    """
    if len(p) <= 4:
        p[0] = IsExpr(Atom('is'), [p[1], p[3]])
    else:
        p[0] = IsExpr(Atom('is'), [p[3], p[5]])

    
def p_atom(p):
    'atom : ATOM'
    p[0] = Atom(p[1])
    
def p_variable(p):
    'variable : VAR'
    p[0] = Var(p[1])
    
def p_number(p):
    'number : NUMBER'
    p[0] = Integer(p[1])
 

# Error rule for syntax errors
def p_error(p):
    print('line', p)
    raise RuntimeError('Syntax error in input !')


# Build the parser
prolog_parser = yacc.yacc()

