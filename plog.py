import operator
from collections import defaultdict
from copy import deepcopy


KB = defaultdict(list)     # knowledge base


class Predicate:
    def __init__(self, pred, rules):
        self.pred = pred
        self.rules = rules
        KB[pred.symbol].append(self)

class Fact(Predicate):
    def __init__(self, pred):
        super().__init__(pred, [])
    def __repr__(self):
        return '<Fact : ' + repr(self.pred) + '>'

class Rule(Predicate):
    def __init__(self, pred, rules):
        super().__init__(pred, rules)
    def __repr__(self):
        return '<Rule : ' + repr(self.pred) + ' :- ' + ', '.join(repr(rule) for rule in self.rules) + '>'


class Goal:
    def __init__(self, pred, level = 0, env = None):
        self.pred = pred
        self.level = level
        self.env = {} if not env else env
        self.vars_syms = {pred.symbol for pred in self.pred.get_vars()}
        # self.env = {k : v for k, v in env.items() if k in self.vars}
    def backtrack(self, rules, env, level):
        if len(rules) == 0:
            yield True, {k : v.rebuild(env) for k, v in env.items()}
        else:
            pred = rules[0].rebuild(env)
            #print(level * '   ', 'current goal is', pred)
            input(level * '   ' + 'continue ?')
            for ok, new_env in Goal(pred.rebuild(env), level, env)._solve():
                if ok:
                    yield from self.backtrack(rules[1:], new_env, level)
                # else :
                    # yield False, new_env
    def _solve(self):
        #print(self.level * '  ' + 'entry', self)
        for term in KB[self.pred.symbol]:
            # print(self.level * '   ', self.pred, '<->', term)
            print(self.level * '   ', 'entry : Goal is', self.pred, 'vars :', self.pred.get_vars())
            print(self.level * '   ', '  -> with ', term)
            new_env = deepcopy(self.env)            
            if self.pred.unify(term.pred, new_env):
                # for var in self.pred.get_vars(): var.unify(var, new_env)
                #print(self.level * '   ', ')env is', new_env)
                # new_env = {k : v.rebuild(dest_env) for k, v in dest_env.items()} # env differs...
                # print(self.level * '   ', '(env is', new_env)
                for ok, dest_env in self.backtrack([rule.rebuild(new_env) for rule in term.rules], {}, self.level + 1):
                    # print(self.level * '   ', ' ... ', term.pred, dest_env)
                    if ok:
                        #print(self.level * '   ', '=> in src :', new_env)
                        #print(self.level * '   ', '=> with dest :', dest_env)
                        new_env = {k : v.rebuild(new_env) for k, v in new_env.items()} # si v est présent dans les 2, ça crée des collisions...
                        new_env = {k : v.rebuild(dest_env) for k, v in new_env.items()}
                        print(self.level * '   ', 'success :', self.pred.rebuild(new_env), '; env :', new_env)
                        yield True, new_env
            else:
                pass
            # print(self.level * '   ', 'quit : ', self.pred, 'env :', new_env)
            # ajouter une clause pour l'arithmétique ?
        else:
            print(self.level * '   ', 'fail :', self.pred, 'env :', new_env)
            yield False, self.env
    # def solve(self):
        # for ok, env in self._solve():
            # tmp = deepcopy(self.env)
            # new_pred = self.pred.rebuild(env)  ## surtout pas !!!!!!!!!!
            # print(self.level * '   ', 'SELF PRED', self.pred, self.pred.get_vars())
            # new_pred.unify(self.pred, self.env)
            # print(self.level * '   ', 'NEW PRED', new_pred, ok, 'env :', env)
            # yield ok, env
            # if ok:
                # if (env): print(self.level * '   ', 'yes : ', env)
                # else: print(self.level * '   ', 'yes : ')
            # else:
                # print(self.level * '   ', 'no : ')
            # print(self.level * '   ', '---- : ', new_pred, ok, 'env :', env)
            # # self.env = tmp
    def __repr__(self):
        return '<Goal : ' + repr(self.pred) + '>'


class Query(Goal):
    def __init__(self, pred):
        super().__init__(pred)
        found = False
        for ok, env in self._solve():
            if ok:
                if (env): self.putenv(env)
                else: print('yes : ')
                found = True
            elif not found:
                print('no : ')
    def putenv(self, env):
        #print('vars : ', self.vars_syms, 'env', {k : v for k, v in env.items() if k in self.vars_syms})
        print(', '.join('{} = {}'.format(k, v) for k, v in env.items() if k in self.vars_syms))
    def __repr__(self):
        return '<Query : ' + repr(self.pred) + ' ? >'


######################################################################

class Term:
    pass

class SimpleTerm(Term):
    pass

class Constant(SimpleTerm):
    def rebuild(self, env):
        return self
    def get_vars(self):
        return set()

class Integer(Constant):
    def __init__(self, val):
        self.val = val
    def eval(self):
        return self.val
    def unify(self, other, env):
        if is_variable(other):
            return other.unify(self, env)
        if is_integer(other):
            # print('unify :' if self.val == other.val else 'not unify :', self, ',', other, env)
            return self.val == other.val
        return False
    def __repr__(self):
        return repr(self.val)

class Atom(Constant):
    def __init__(self, symbol):
        self.symbol = symbol
    def unify(self, other, env):
        if is_variable(other):
            return other.unify(self, env)
        if is_atom(other):
            # print('unify :' if self.symbol == other.symbol else 'not unify :', self, ',', other, env)
            return self.symbol == other.symbol
        return False
    def __repr__(self):
        return self.symbol
        
class Op(Atom):
    def __init__(self, symbol):
        super().__init__(symbol)

def rec_get(var, env):
    return rec_get(env[var.symbol], env) if var.symbol in env else var
        
class Var(SimpleTerm):
    def __init__(self, symbol):
        self.symbol = symbol
    def get_vars(self):
        return set([self])
    def eval(self):
        raise RuntimeError('Unbounded var')
    def unify(self, other, env):
        if self is other:
            env[self.symbol] = rec_get(self, env)
            return True
        elif self.symbol not in env:
            if not (is_variable(other) and other.symbol not in env):   # sauf si other est une variable libre
                env[self.symbol] = other if not is_variable(other) else rec_get(other, env)                   
            # print('unify :', self, ',', other, env)
            # return self.val.unify(other, env)
            return True
        elif is_variable(other):
            return other.unify(self, env)
        return env[self.symbol].unify(other, env)
    def rebuild(self, env):
        return env.get(self.symbol, self)
    def __repr__(self):
        return self.symbol

class ComplexTerm(Term):
    def __init__(self, atom, lst):
        self.atom = atom
        self._symbol = atom.symbol
        self.arity  = len(lst)
        self.symbol = self._symbol + '/' + repr(self.arity)
        self.lst    = lst
    def unify(self, other, env):
        if is_variable(other):
            return other.unify(self, env)
        if is_complex(other):
            if self.symbol != other.symbol:
                # print('no unify (1. different functors) :', self, ',', other, env)
                return False
            for term1, term2 in zip(self.lst, other.lst):
                if not term1.unify(term2, env):
                    # print('no unify (2. terms not unified) :', self, ',', other, env)
                    return False
            # print('unify :', self, ',', other, env)
            return True
        # print('no unify (3. not same type) :', self, ',', other, env)
        return False
    def rebuild(self, env):
        return type(self)(self.atom, [term.rebuild(env) for term in self.lst])
    def get_vars(self):
        ret = set()
        for term in self.lst:
            ret.update(term.get_vars())
        return ret
    def __repr__(self):
        return self._symbol + '(' + ', '.join(repr(e) for e in self.lst) + ')'

class List(ComplexTerm):
    def __init__(self, atom, lst = None):
        super().__init__(atom, lst)
        assert(self.arity == 2)
    def to_list(self):      # convert to builtin list
        return [self.lst[0]] + (self.lst[1].to_list() if is_list(self.lst[1]) else [self.lst[1]])
    def __repr__(self):
        ls = self.to_list()
        return '[' + ', '.join(repr(e) for e in ls[:-1]) + ('' if ls[-1] is nil else (' | ' + repr(ls[-1]))) + ']'

class IsExpr(ComplexTerm):
    def __init__(self, atom, lst):
        super().__init__(atom, lst)
        self.var = lst[0]
        self.expr = lst[1]
    def unify(self, other, env):
        if not is_variable(self.var):
            return False
        else:
            return self.var.unify(self.expr.eval(), env)

        
class ArithExpr(ComplexTerm):
    op_table = {
        '+' : operator.add,
        '-' : operator.sub,
        '*' : operator.mul,
        '/' : operator.floordiv
        }
    def __init__(self, atom, lst):
        super().__init__(atom, lst)
        self.op = ArithExpr.op_table[atom.symbol]
    def eval(self):
        return Number(self.op(self.lst[0].eval(), self.lst[1].eval()))
        
class BoolExpr(ComplexTerm):
    op_table = {
        '<' : operator.lt,
        '=<' : operator.le,
        '=:=' : operator.eq, 
        '=\=' : operator.ne,
        '>=' : operator.ge, 
        '>' : operator.gt,
        }
    def __init__(self, atom, lst):
        super().__init__(atom, lst)
        self.op = BoolExpr.op_table[atom.symbol]
    def eval(self):
        r = self.op(self.lst[0].eval(), self.lst[1].eval())
        return Atom('true') if r else Atom('false')


def is_list(obj):
    return isinstance(obj, List)

def is_variable(obj):
    return isinstance(obj, Var)

def is_integer(obj):
    return isinstance(obj, Integer)
    
def is_atom(obj):
    return isinstance(obj, Atom)

def is_complex(obj):
    return isinstance(obj, ComplexTerm)    

def is_rule(obj):
    return isinstance(obj, Rule) 

def is_fact(obj):
    return isinstance(obj, Fact)
    
def is_query(obj):
    return isinstance(obj, Query)

fail = Atom('fail')
nil = Atom('[]')

##################################################################
