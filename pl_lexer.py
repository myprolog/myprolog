import ply.lex as lex

reserved = {
   'is' : 'IS'
}

# List of token names.
tokens = [
    'VAR',
    'ATOM',
    'NUMBER',
    'SEP',
    'EQL',
    'NEQL',
    'LT',
    'LE',
    'GT',
    'GE',
] + list(reserved.values())

literals = [ ',', '[', ']', '(', ')', '|', '.', '?', '+', '-', '/', '*' ]



# Regular expression rules for simple tokens
t_VAR  = r'[A-Z_][A-Za-z0-9_]*'
t_SEP  = r':-'
t_EQL  = r'=:='
t_NEQL = r'=\='
t_LT   = r'<'
t_LE   = r'=<'
t_GT   = r'>'
t_GE   = r'>='


# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'
t_ignore_COMMENT = r';.*'

def t_ATOM(t):
    r'[a-z][A-Za-z0-9_]*'
    t.type = reserved.get(t.value,'ATOM')    # Check for reserved words
    return t

# A regular expression rule with some action code
# Note addition of self parameter since we're in a class
def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)    
    return t

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# Error handling rule
def t_error(t):
    raise RuntimeError("Illegal character '%s' at line %d" % (t.value[0], t.lexer.lineno))

# Build the lexer
prolog_lexer = lex.lex()

