from pl_lexer import prolog_lexer
from pl_parser import prolog_parser
from plog import KB


prgm1 = """
pere(marc,bob).
pere(dan,bob).
fils(X,Y) :- pere(Y,X).
fils(bob,X)?
"""   # Test it
#t = m.get_tokens("[1,2,3 | [4,5]]")
#print(t)

prgm2 = """
pere(bob,jean).
"""

prgm3 = """
pere(X,jean).
"""

prgm4 = """
X is 3.
X > 2?
"""

prgm5 = """
member(X, [X|T]).
member(X, [H|T]) :- member(X, T).

member(1, [2,1,3]) ?
member(X, [a,b,c]) ?
; member(0, [1,2,3]) ?
"""

prgm6 = """
append([], L, L).
append([H|X], Y, [H|Z]) :- append(X, Y, Z).

append([a, b], [c, d], L3) ?
"""

# member(X, [X|T]).
# member(X, [H|T]) :- member(X, T).



#re1 = prolog_parser.parse(prgm1, lexer=prolog_lexer)
#print(re1)

#re2 = prolog_parser.parse(prgm2, lexer=prolog_lexer)
#print(re2)

#re3 = prolog_parser.parse(prgm3, lexer=prolog_lexer)
#print(re3)

#re4 = prolog_parser.parse(prgm4, lexer=prolog_lexer)
#print(re4)

#re5 = prolog_parser.parse(prgm5, lexer=prolog_lexer)
#print(re5)

re6 = prolog_parser.parse(prgm6, lexer=prolog_lexer)
#print(re6)

#E = {}
#print(re2[0].pred.unify(re3[0].pred, E))
#print(E)



#print(KB)


